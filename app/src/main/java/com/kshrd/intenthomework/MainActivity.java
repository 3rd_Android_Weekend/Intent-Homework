package com.kshrd.intenthomework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvUserName = (TextView) findViewById(R.id.tvUserName);
        Button btnLogout = (Button) findViewById(R.id.btnLogout);

        username = getIntent().getStringExtra("USERNAME");
        tvUserName.setText("Welcome " + username);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        Button btnSecondApp = (Button) findViewById(R.id.btnSecondApp);
        btnSecondApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(MyIntent.KSHRD);
                i.putExtra("USERNAME", username);

                if (i.resolveActivity(getPackageManager()) != null){
                    startActivity(i);
                }
            }
        });


    }
}
