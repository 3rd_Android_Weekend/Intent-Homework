package com.kshrd.intenthomework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    EditText etUserName;
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(etUserName.getText().toString(), etPassword.getText().toString());
            }
        });
    }

    void initView() {
        btnLogin = (Button) findViewById(R.id.btnLogin);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    void login(String username, String password) {
        String user = "admin", pwd = "12345abc";

        if (user.equals(username) && pwd.equals(password)){
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("USERNAME", username);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(this, "Invalid Information", Toast.LENGTH_SHORT).show();
        }
    }
}
